FROM alpine:3.15
WORKDIR /app
COPY todosync /usr/local/bin
RUN chmod 755 /usr/local/bin/todosync
