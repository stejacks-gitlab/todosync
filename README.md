## TODOsync

Creates TODOs on .com for a remote GitLab instance. Because there is no current support to create arbitrary TODO items, this implements a hack using self mentions on a project (where it is run).

**Note**: This suddenly has become a lot less useful due to https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112939#note_1302249090 where it is no longer possible to click on individual links in TODOs.

### Features

- When run, will create an issue for every TODO on ops with a note that self mentions, so a corresponding TODO is created on .com.
- When the TODO is resolved on ops, the issue is closed, removing the TODO on .com.
- If the TODO is resolved on .com, but not on ops, it will be re-created via another note on the corresponding issue

### Configuration

- `GITLAB_API_TOKEN` (required): API token for creating issues and issue notes
- `GITLAB_OPS_API_TOKEN` (required): API token for reading TODOs on ops.gitlab.net
- `GITLAB_USER_LOGIN` (optional): Username for mentions, in CI defaults to the user running the CI job
- `CI_PROJECT_PATH` (optional): Project path, in CI defaults to this project

### Instructions

1. Fork this project
2. In the CI config set the following variables
    - Set `GITLAB_API_TOKEN` with a new token value, by [creating a new personal access token](https://gitlab.com/-/profile/personal_access_tokens) with the `api` box checked
    - Set `GITLAB_OPS_API_TOKEN` with a new token value, by [creating a new personal access token](https://ops.gitlab.net/-/profile/personal_access_tokens) with the `read_api` box checked
3. Trigger a pipeline once to create an image that will be used later in the scheduled pipeline: `Build > Pipelines > Run pipeline`.
4. Create a CI schedule that runs every 5 minutes
