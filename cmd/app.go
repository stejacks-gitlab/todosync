package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"github.com/xanzy/go-gitlab"
	"go.uber.org/zap"
)

const GITLAB_OPS_API_URL string = "https://ops.gitlab.net/api/v4"
const TODO_LABEL string = "todoSync"

type App struct {
	username    string
	todoProject string
	logger      logr.Logger
	gitLab      *gitlab.Client
	gitLabOps   *gitlab.Client
	dryRun      bool
}

func NewApp() (*App, error) {
	username := os.Getenv("GITLAB_USER_LOGIN")
	if username == "" {
		return nil, errors.New("GITLAB_USER_LOGIN not set")
	}

	todoProject := os.Getenv("CI_PROJECT_PATH")
	if todoProject == "" {
		return nil, errors.New("CI_PROJECT_PATH not set")
	}

	token := os.Getenv("GITLAB_API_TOKEN")
	if token == "" {
		return nil, errors.New("GITLAB_API_TOKEN not set")
	}

	tokenOps := os.Getenv("GITLAB_OPS_API_TOKEN")
	if tokenOps == "" {
		return nil, errors.New("GITLAB_OPS_API_TOKEN not set")
	}

	_, dryRun := os.LookupEnv("DRY_RUN")

	gitLab, err := gitlab.NewClient(token)
	if err != nil {
		return nil, fmt.Errorf("new gitlab client: %w", err)
	}

	gitLabOps, err := gitlab.NewClient(tokenOps, gitlab.WithBaseURL(GITLAB_OPS_API_URL))
	if err != nil {
		return nil, fmt.Errorf("new gitlab ops client: %w", err)
	}

	zapLog, err := zap.NewProduction(zap.Fields(zap.Int("pid", os.Getpid())))
	if err != nil {
		return nil, fmt.Errorf("new application logger: %w", err)
	}

	defer zapLog.Sync() //nolint:errcheck
	logger := zapr.NewLogger(zapLog)

	return &App{
		username:    username,
		todoProject: todoProject,
		logger:      logger,
		gitLab:      gitLab,
		gitLabOps:   gitLabOps,
		dryRun:      dryRun,
	}, nil
}

func (a *App) Run() error {
	todos, err := a.FetchTodos(a.gitLab)
	if err != nil {
		return err
	}

	opsTodos, err := a.FetchTodos(a.gitLabOps)
	if err != nil {
		return err
	}

	todoIssues, err := a.ListTodoIssues()
	if err != nil {
		return err
	}

	for _, todo := range opsTodos {
		if a.todoPresent(todos, fmt.Sprint(todo.ID)) {
			a.logger.Info("TODO already present, not doing anything", "ID", todo.ID, "targetURL", todo.TargetURL)
			continue
		}

		issue, err := a.CreateTodoIssue(todo.ID, todoIssues)
		if err != nil {
			return err
		}

		if err := a.CreateTodoNote(issue, string(todo.ActionName), todo.TargetURL); err != nil {
			return err
		}
	}

	if err := a.todoCleanup(opsTodos, todoIssues); err != nil {
		return err
	}

	return nil
}

func (a *App) todoCleanup(opsTodos []*gitlab.Todo, todoIssues []*gitlab.Issue) error {
	for _, issue := range todoIssues {
		todoID, err := strconv.Atoi(issue.Title)
		if err != nil {
			a.logger.Info("Unable to parse issue title for todo ID!", "title", issue.Title)
			return fmt.Errorf("todoCleanup: %w", err)
		}

		if !a.todoIDPresent(opsTodos, todoID) {
			a.logger.Info("Todo no longer present for open issue, closing", "webURL", issue.WebURL, "issueTitle", issue.Title)
			_, _, err := a.gitLab.Issues.UpdateIssue(a.todoProject, issue.IID, &gitlab.UpdateIssueOptions{StateEvent: strptr("close")})
			if err != nil {
				return fmt.Errorf("todoCleanup: %w", err)
			}
		}
	}
	return nil
}

func (a *App) todoPresent(todos []*gitlab.Todo, todoTitle string) bool {
	for _, todo := range todos {
		if todo.Target.Title == todoTitle {
			return true
		}
	}
	return false
}

func (a *App) todoIDPresent(todos []*gitlab.Todo, todoID int) bool {
	for _, todo := range todos {
		if todo.ID == todoID {
			return true
		}
	}
	return false
}

func (a *App) FetchTodos(client *gitlab.Client) ([]*gitlab.Todo, error) {
	opts := &gitlab.ListTodosOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	todos := []*gitlab.Todo{}

	for {
		t, resp, err := client.Todos.ListTodos(opts)
		if err != nil {
			return nil, fmt.Errorf("FetchTodos: %w", err)
		}

		todos = append(todos, t...)

		if resp.NextPage == 0 {
			break
		}

		opts.Page = resp.NextPage
	}

	return todos, nil
}

func (a *App) CreateTodoIssue(todoID int, todoIssues []*gitlab.Issue) (*gitlab.Issue, error) {
	// See if an issue was already created, if so return the one that is open
	for _, todoIssue := range todoIssues {
		if todoIssue.Title == fmt.Sprint(todoID) {
			a.logger.Info("Using existing issue to create a TODO", "webURL", todoIssue.WebURL)
			return todoIssue, nil
		}
	}

	opts := &gitlab.CreateIssueOptions{
		Title:  strptr(fmt.Sprint(todoID)),
		Labels: &gitlab.Labels{TODO_LABEL},
	}

	if a.dryRun {
		a.logger.Info("Would have created issue", "title", fmt.Sprint(todoID))
		return nil, nil
	}

	i, _, err := a.gitLab.Issues.CreateIssue(a.todoProject, opts)

	if err != nil {
		return nil, fmt.Errorf("CreateTodoIssue: %w", err)
	}

	a.logger.Info("Created todo issue", "webURL", i.WebURL)
	return i, nil
}

func (a *App) CreateTodoNote(issue *gitlab.Issue, actionName, targetURL string) error {

	noteBody := fmt.Sprintf("@%s: [**%s** on ops](%s)", a.username, actionName, targetURL)

	if issue == nil || a.dryRun {
		a.logger.Info("Would have created note", "noteBody", noteBody)
		return nil
	}

	a.logger.Info("Adding note to issue to generate a TODO", "webURL", issue.WebURL)

	opts := &gitlab.CreateIssueNoteOptions{
		Body: strptr(noteBody),
	}

	_, _, err := a.gitLab.Notes.CreateIssueNote(a.todoProject, issue.IID, opts)

	if err != nil {
		return fmt.Errorf("CreateTodoNote: %w", err)
	}

	return nil
}

func (a *App) ListTodoIssues() ([]*gitlab.Issue, error) {
	opts := &gitlab.ListProjectIssuesOptions{
		State:  strptr("opened"),
		Labels: &gitlab.Labels{TODO_LABEL},
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	issues := []*gitlab.Issue{}

	for {
		i, resp, err := a.gitLab.Issues.ListProjectIssues(a.todoProject, opts)
		if err != nil {
			return nil, fmt.Errorf("ListTodoIssues: %w", err)
		}

		issues = append(issues, i...)

		if resp.NextPage == 0 {
			break
		}
		opts.Page = resp.NextPage

	}

	return issues, nil
}

func strptr(s string) *string {
	return &s
}
