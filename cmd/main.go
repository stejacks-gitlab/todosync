package main

import (
	"log"
	"os"
)

func main() {
	app, err := NewApp()
	if err != nil {
		log.Printf("starting application failed: %v\n", err)
		os.Exit(1)
	}

	err = app.Run()
	if err != nil {
		log.Printf("running application failed: %v\n", err)
		os.Exit(1)
	}
}
